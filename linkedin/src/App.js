import React, { useEffect } from 'react';
import { useDispatch, useSelector  } from 'react-redux';
import './App.css';
import Header from './Header';
import Sidebar from './Sidebar';
import { auth } from './firebase'; 
import Feed from './Feed';
import { login, logout, selectUser } from './features/userSlice';
import Login from './Login';
import Widgets from './Widgets';

function App() {
  // const user = useSelector(selectUser)
  const user = useSelector(selectUser)
  const dispatch = useDispatch();

  useEffect(() => {
    auth.onAuthStateChanged((userAuth) => {
      if (userAuth) {
        dispatch(
          login({
            email: userAuth.user.email,
            uid: userAuth.user.uid,
            displayName: userAuth.displayName,
            photoUrl: userAuth.photoURL,
          })
        );
      } else {
        dispatch(logout());
      }
    })
  }, []);
  return (
    <div className="app">
      {/* Header */}
      <Header />
      {!user ? (<Login />) : (
        <div className='app_body'>
        <Sidebar />
        <Feed />
      <Widgets />
      </div>
    
      )}
      </div>
  );
}

export default App;
