import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyBylV8fhjCCWO0wplD2JzDIMI6siq28K8I",
  authDomain: "linkedin-7a922.firebaseapp.com",
  projectId: "linkedin-7a922",
  storageBucket: "linkedin-7a922.appspot.com",
  messagingSenderId: "193496910759",
  appId: "1:193496910759:web:ebbd5f3d8ee8bdf1400482",
  measurementId: "G-GPBECFGDJR"
};

  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const db = firebase.firestore();
  const auth = firebase.auth();

  export { db, auth };