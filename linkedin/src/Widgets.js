import React from 'react';
import './Widgets.css';
import InfoIcon from '@mui/icons-material/Info';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

function Widgets() {
    const newsArticle = (heading, subtitle) => (
        <div className='widgets_article'>
            <div className='widgets_articleLeft'>
                <FiberManualRecordIcon />
            </div>
            <div className='widgets_articleRight'>
                <h4>{heading}</h4>
                <p>{subtitle}</p>
            </div>
        </div>
    )
  return (
    <div className='widgets'>
      <div className='widgets_header'>
          <h2>LinkedIn News</h2>
          <InfoIcon />
      </div>
      {newsArticle("Fast Food & Takeaway", "Perfect epos solution to empower you to manage your retail or hospitality business.")}
      {newsArticle("Software For Schools ", "Impress Your Visitors From The Moment They Walk Into Reception. Contactless Available.")}
      {newsArticle("Odoo Inventory", "Track every stock move from purchase to warehouse bin to sales order. Trace lots and serial numbers everywhere.")}
      {newsArticle("POS SYSTEM THAT TRACKS INVENTORY", "Search thousands of job openings in United Kingdom, GB. Find the best full-time, part-time, hourly, and remote jobs on receptix.")}
     

    </div>
  )
}

export default Widgets
